import { Component, OnInit } from '@angular/core';
import {Observable, Observer} from 'rxjs';

@Component({
  selector: 'app-pipe',
  template: `
    <!--<div class="container">-->
      <!--<h2>Pipe</h2>-->
      <!--<p>{{name}} {{name | uppercase}} {{name | lowercase}} {{name | slice:0:2}} {{name | slice:1:3 | uppercase}}</p>-->
      <!--<p>{{pi}} {{pi | number}} {{pi | number:'1.2-2'}} {{pi | number:"3.2-2"}} {{name | slice:1:3 | uppercase}}</p>-->
      <!--<p>{{amount}} {{amount | currency:"EUR"}} {{amount | currency:"USD"}} {{name | slice:0:2}} {{name | slice:1:3 | uppercase}}</p>-->
      <!--<p>{{date}} {{date | date}} {{date | date:'short':"ru-BY"}} {{name | slice:0:2}} {{name | slice:1:3 | uppercase}}</p>-->
      <!--<p>{{pr}} {{pr | percent}} {{name | lowercase}} {{name | slice:0:2}} {{name | slice:1:3 | uppercase}}</p>-->
      <!--<p>{{obj}} {{obj | json}} {{name | lowercase}} {{name | slice:0:2}} {{name | slice:1:3 | uppercase}}</p>-->
      <!--<p>{{myP}} {{myP | mypipe}} {{myP | mypipe:3}} {{myP | mypipe:4}} {{10 | mypipe:5}}</p>-->
    <!--</div>-->
    <h1>{{asyncTitle }} {{asyncTitle |async}}</h1>
    <input type="text" class="form-control" [(ngModel)]="searchCar" />
    <button class="btn btn-primary" (click)="addCar()">Добавить</button>
    <hr />
    <ul class="list-group">
      <li class="list-group-item" *ngFor="let car of cars | pipeCar:searchCar:'name';  let i = index">
        <b>{{i+1}}. </b> {{car.name}}
      </li>
    </ul>
  `,
  styleUrls: ['./pipe.component.css']
})
export class PipeComponent implements OnInit {
  constructor() { }
  name = 'Pipe';
  searchCar = '';
  pi = Math.PI;
  amount = 350;
  myP = 2;
  date = new Date();
  pr = 0.45;
  obj = {
    bar  : 2,
    bss : 3,
    ba: [{b: 'jha', dd : 'fs' }, {b : 'kdsgfvk', dd : 'dfsddfzxghb'}]
  };
  cars = [{name: 'Audi'}, {name: 'Ford'}, {name: 'BMW'}, {name: 'Mazda'}, {name: 'Opel'}];

  asyncTitle = new Observable<string>((observer: Observer<string>) => {
    setInterval(() => observer.next("Care ansync"), 2000);
  });
  addCar() {
    this.cars.push({
      name: 'New car'
    });
  }
  ngOnInit() {
  }

}
