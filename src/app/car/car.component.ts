import {
  AfterContentChecked,
  AfterContentInit,
  AfterViewChecked,
  AfterViewInit,
  Component, ContentChild, DoCheck, ElementRef,
  Input,
  OnChanges,
  OnDestroy,
  OnInit, SimpleChanges
} from '@angular/core';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  // styleUrls: ['./car.component.css']
  styles : [`
    h1 {
      border: 1px solid  saddlebrown;
    }
  `]
})
export class CarComponent implements OnInit,
  OnChanges,
  AfterContentInit,
  AfterContentChecked,
  AfterViewChecked,
  AfterViewInit,
  DoCheck,
  OnDestroy {
  year = 2015;
 // @Input () carItem: {name:string, year:number};
  @Input ('carItem') car: {name: string, year: number};
  @Input() name: string;
  @ContentChild ('carHeadihg') carHeadihg: ElementRef;
  constructor () {
    console.log("constructor");
  }
  ngOnInit() {
    console.log("ngOnInit");
  }
  ngOnChanges(changes: SimpleChanges) { console.log("ngOnChanges",changes); }
  ngDoCheck() { console.log("ngDoCheck");}
  ngAfterContentChecked() { console.log("ngAfterContentChecked");}
  ngAfterContentInit() { console.log("ngAfterContentInit");}
  ngAfterViewChecked() { console.log("ngAfterViewChecked");}
  ngAfterViewInit() {
    console.log(this.carHeadihg);
  }
  ngOnDestroy(){
   console.log("ngOnDestroy");
  }
}


