import {Component, Input, OnInit} from '@angular/core';
import {ConsoleService} from '../console.service';

@Component({
  selector: 'app-auto',
  templateUrl: './auto.component.html',
  styleUrls: ['./auto.component.css'],
 // providers: [ConsoleService]
})
export class AutoComponent implements OnInit {
  constructor (private cL: ConsoleService) {}
  @Input ('carItem') car;
  constructor() { }
  bgColor (){
     return !this.car.isSold ? 'list-group-item list-group-item-success' : 'list-group-item list-group-item-danger';
  }
  action(str: string) {
    this.car.isSold = str === 'bay' ? true : false;
    this.cL.logConsole(str);
  }

  ngOnInit() {
  }

}
