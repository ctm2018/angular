import {Directive, ElementRef, HostBinding, HostListener, Input, OnInit, Renderer2} from '@angular/core';

@Directive({
  selector: '[appMyDirective]'
})
export class MyDirectiveDirective implements OnInit {
  @HostBinding ('style.backgroundColor') background: string;
  @Input ('appMyDirective') hoverColor: string;
  @Input () defaultColor: string;

  constructor(private element: ElementRef, private renderer: Renderer2) {}
  ngOnInit() {
     this.background = this.defaultColor;
   // const {nativeElement} = this.element;
   // nativeElement.style.backgroundColor='red';
    // this.renderer.setStyle(nativeElement,'background-color', 'blue');
    // this.renderer.addClass(nativeElement,'white-text');
  }
  @HostListener('mouseenter') mouseEnter () {
    this.background = this.hoverColor;
     // this.background = "blue";
   // const {nativeElement} = this.element;
    // nativeElement.style.backgroundColor='red';
    // this.renderer.setStyle(nativeElement,'background-color', 'blue');
  }
  @HostListener('mouseleave') mouseLeave () {
     this.background = this.defaultColor;
    // this.background = 'white';
    // const {nativeElement} = this.element;
    // nativeElement.style.backgroundColor='red';
   // this.renderer.setStyle(nativeElement,'background-color', 'white');
  }

}
