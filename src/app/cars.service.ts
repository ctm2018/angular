import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {catchError, delay, map} from 'rxjs/internal/operators';
import {Observable, throwError} from 'rxjs';


@Injectable()
export  class  CarsService {
  //   constructor(private service: ConsoleService) {}
  //   cars = [{name: "Ford", isSold: false },
  //   {name: "Audi", isSold: true },
  //   {name: "Mazda", isSold: false }];
  //   addCar(name: string) {
  //   this.cars.push({name, isSold: false});
  //   this.service.logConsole(`Машина  ${name} была добавлена`);
  // }
  constructor(private http: Http){}
  getCars() {
   return this.http.get('http://localhost:3000/cars').pipe(map((response: Response) => response.json()),
     catchError(() => {return throwError("Сервер сдох")}));
  }
  getDetaliCars(id: number) {
    return this.http.get(`http://localhost:3000/cars/${id}`).pipe(map((response: Response) => response.json()), map((data) => data),
      catchError(() => {return throwError("Сервер сдох")}));
  }
  getTitle() {
    return this.http.get('http://localhost:3000/title').pipe(delay(3000),map((response: Response) => response.json()), map((data) => data.value),
      catchError(() => {return throwError("Сервер сдох")}));
  }

  addCars(data) {
    return this.http.post('http://localhost:3000/cars',data).pipe(map((response: Response) => response.json()));
  }

  updateCars(car: any, color: string) {
    car.color = color;
    return this.http.put(`http://localhost:3000/cars/${car.id}`,car).pipe(map((response: Response) => response.json()));
  }
  deleteCars(car: any){
    return this.http.delete(`http://localhost:3000/cars/${car.id}`).pipe(map((response: Response) => response.json()));
  }

}
