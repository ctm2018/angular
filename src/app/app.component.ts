import {Component, OnInit, ViewChild} from '@angular/core';
import {CarsService} from './cars.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
// import {FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
// import {Observable, Observer} from 'rxjs';
interface  Cars {
  name:string; year:number; id:number; color:string;
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  // styles: [`input.ng-invalid.ng-touched{ border: 1px solid red; }`]
})

export class AppComponent implements OnInit {
  // @ViewChild ('form') form: NgForm;
  title = 'projectAngular';
 // items = [1, 2, 3, 4, 5];
  // formData = {};
  // isSubmit ="";
  // radios = [{name: 'Да', num: 1}, {name: 'Нет', num: 0}];
  // current = 1;
  // defaultCountry="by";
  // defaultRadio = 0;
  // onClick(number: number) {
  //   this.current = number;
  // }
  // dataForm() {
  //   console.log("Submited!", this.form);
  //   this.isSubmit = this.form.submitted;
  //   this.formData = this.form.value;
  //   this.form.reset();
  // }
  // addEmail() {
  //   this.form.form.patchValue({
  //     user: {email: "wbf@gmail.com"}
  //   });
  //
  // lengthPass = 5;
   form: FormGroup;
  //
  // ngOnInit() {
  //   this.form = new FormGroup(
  //     {
  //       user: new FormGroup({
  //         email: new FormControl('', [Validators.required, Validators.email], this.isUser),
  //         pass: new FormControl('', [Validators.required, this.onCheckPassLehgth.bind(this)])
  //       }),
  //       country: new FormControl('by'),
  //       answer: new FormControl(0)
  //     }
  //   );
  // }
  //
  // onSubmit() {
  //   console.log(this.form);
  // }
  //
  // onCheckPassLehgth(control: FormControl) {
  //   if (control.value.length <= this.lengthPass) {
  //     return {
  //       errorLength: true
  //     };
  //   }
  //   return null;
  // }
  // isUser(control: FormControl):  Promice<any> {
  //   return new Promise((resolve, reject) => {
  //     setTimeout(() => {
  //       if (control.value === 'marklash13@gmail.com') {
  //         resolve({
  //           isEmailUsed : true
  //         });
  //       } else {
  //         resolve(null);
  //       }
  //     }, 3000);
  //   });
  // }
  titleApp;
  constructor(private carsService: CarsService) {}
  cars: any;
  colors = ['red', 'green', 'blue', 'pink'];
  randomColor () {
  const num =  Math.round(Math.random() * (this.colors.length - 1));
  return this.colors[num];
  }
  loader() {
    this.cars = this.carsService.getCars();
    // this.carsService.getCars().subscribe((responce: Response) => {
    //   //console.log(responce);
    //   this.cars = responce.json();
    // });

    // this.carsService.getCars().subscribe((cars: Cars[]) => {
    //   //console.log(responce);
    //   this.cars = cars;
    // }, (error) => {alert(error);}
    // );
  }

  ngOnInit() {
    this.form = new FormGroup(
      {
        cars: new FormGroup({
          name: new FormControl('', Validators.required),
          color: new FormControl('', Validators.required),
          year: new FormControl('', [Validators.required, Validators.pattern('^\\d{4}$')])
        })});
       this.titleApp = this.carsService.getTitle();
  }

  onSubmit() {
    console.log(this.form);
    if (!this.form.invalid) {
      this.carsService.addCars(this.form.value.cars).subscribe((car: Cars[]) => {
        this.cars.push(car);
      });
      this.form.reset();
    }
  }

  updateColor(car: Cars) {
    this.carsService.updateCars(car, this.randomColor()).subscribe((data) => {
       console.log(data);
    });
  }

  deleteCar(car: Cars) {
    this.carsService.deleteCars(car).subscribe((data) => {
      this.cars = this.cars.filter((c) => c.id !== car.id);
    });
  }
}
