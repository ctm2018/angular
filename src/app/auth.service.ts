export class AuthService {
  isLoged = false;

  isAuth() {
    return new Promise((resolve, reject) => {
      setTimeout(() => resolve(this.isLoged),1000); } )
  }
  loginIn () {
    this.isLoged = true;
  }
  loginOut () {
    this.isLoged = false;
  }
}
