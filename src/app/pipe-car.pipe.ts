import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pipeCar',
  pure: false
})
export class PipeCarPipe implements PipeTransform {

  transform(carLisr, search: string, filedName: string ) {
    if(carLisr.length === 0 || search ==='') {
      return carLisr;
    }
    return carLisr.filter((car) => car[filedName].toLowerCase().indexOf(search.toLowerCase()) !== -1);
  }

}
