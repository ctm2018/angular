import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {CarsService} from '../cars.service';


@Component({
  selector: 'app-car-page',
  templateUrl: './car-page.component.html',
  styleUrls: ['./car-page.component.css']
})
export class CarPageComponent implements OnInit {
//  car: any = {};
  id: number;
  year: string;
  name: string;
  color: string;
  hag: string;

  constructor(private route: ActivatedRoute, private carsService: CarsService, private router: Router) {
  }

  ngOnInit() {
   this.id = +this.route.snapshot.params['id'];
    // this.id = this.route.snapshot.params['id'];
    this.name = this.route.snapshot.params['name'];
    this.year = this.route.snapshot.queryParams['year'];
    this.color = this.route.snapshot.queryParams['color'];
    this.hag = this.route.snapshot.fragment;
    // this.route.params.subscribe((params: Params) => {

    // this.carsService.getDetaliCars(this.id).subscribe((data) => {
    //   console.log(data);
    //   this.car = data;
    // });
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.name = params['name'];
    });
    this.route.queryParams.subscribe((params: Params)=>{
      this.color = params['color'];
      this.year =  params['year'];
    });
    this.route.fragment.subscribe((params: Params)=>{
      this.hag = params;
    });


  }
  openPageMazda() {
    this.router.navigate(['/cars', 8, 'Mazda'], {queryParams : {
      year: 1999,
        color: "gray"
      }, fragment: 'mazda'} );
  }
}
