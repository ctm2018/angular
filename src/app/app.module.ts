import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CarsComponent } from './cars/cars.component';
import { CarComponent } from './car/car.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AddCarComponent } from './add-car/add-car.component';
import { MyDirectiveDirective } from './derectives/my-directive.directive';
import { PipeComponent } from './pipe/pipe.component';
import { MypipePipe } from './mypipe.pipe';
import { PipeCarPipe } from './pipe-car.pipe';
import { AutosComponent } from './autos/autos.component';
import { AutoComponent } from './auto/auto.component';
import {ConsoleService} from './console.service';
import {HttpModule} from '@angular/http';
import {CarsService} from './cars.service';
import { CarsPageComponent } from './cars-page/cars-page.component';
import { HomePageComponent } from './home-page/home-page.component';
import {RouterModule, Routes} from '@angular/router';
import { CarPageComponent } from './car-page/car-page.component';
import { NotFoundComponent } from './not-found/not-found.component';
import {AuthService} from './auth.service';
import {AuthGuardService} from './auth-guard.service';
import { NewPageComponent } from './new-page/new-page.component';


@NgModule({
  declarations: [
    AppComponent,
    CarsComponent,
    CarComponent,
    AddCarComponent,
    MyDirectiveDirective,
    PipeComponent,
    MypipePipe,
    PipeCarPipe,
    AutosComponent,
    AutoComponent,
    CarsPageComponent,
    HomePageComponent,
    CarPageComponent,
    NotFoundComponent,
    NewPageComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpModule,
  ],
  providers: [ConsoleService, CarsService, AuthService, AuthGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
