import { Component, OnInit } from '@angular/core';
import {CarsService} from '../cars.service';

@Component({
  selector: 'app-autos',
  templateUrl: './autos.component.html',
  styleUrls: ['./autos.component.css'],
  providers: [CarsService]
})
export class AutosComponent implements OnInit {
  inputText ='';
  cars = [];
  constructor(private service: CarsService) { }

  ngOnInit()  {  this.cars = this.service.cars; }

  addCar() {
    this.service.addCar(this.inputText);
    this.inputText ='';
  }
}
