import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: '[app-cars]', // . []
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css'],
  // encapsulation: ViewEncapsulation.Native
})
export class CarsComponent {
  carAdd = false;
  carStatus = false;
  cars: {name: string, year: number}[] = [{
    name: "Audi", year: 2015}
    // {name: "Audi", year: 2015},
    // {name: "Audi", year: 2015}
    ];
  items = [{id:3, name: 'Suka'}, {id:6, name: 'Buka'}, {id:9, name: 'Muka'}];
  carName='';
  carYear="";
  constructor() {
    // setTimeout(() => {
    //   this.carAdd = true;
    // }, 4000);
  }
  updateCar(car: {name: string, year: number}) {
    this.cars.push(car);
  }
  // inputValue(event) {
  //   this.iV = event.target.value;
  // }
  bigText(car: string){
    return car.length > 4 ? true : false;
  }
  updateBar() {
    console.log("Update");
    this.cars[0].name = "Ford";
  }
  deleteCar(){
    this.cars.splice(0,1);
  }
}
