import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';

@Component({
  selector: 'app-add-car',
  templateUrl: './add-car.component.html',
  styleUrls: ['./add-car.component.css']
})
export class AddCarComponent implements OnInit {

  // carName='';
  // carYear=2018;
  @Output ('onAddCar') emit = new EventEmitter<{name: string, year: number}>();
  @ViewChild ('carYear') carYear;

  addCar(carNameEl) {
    //  console.log(this.carYear);
   this.emit.emit({name: carNameEl.value, year: +this.carYear.nativeElement.value});
    // this.carName="";
    // this.carYear=2018;
    carNameEl.value = "";
    this.carYear.nativeElement.value = 2018;
  }

  ngOnInit() {
  }

}
